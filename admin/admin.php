<?php

add_action("admin_menu", "bspkn_form_menu");
    function bspkn_form_menu(){
        add_menu_page( "Bspkn Inscription Form", "Bspk Form", "manage_options", "bspkn-form", "bspkn_form_admin", "dashicons-feedback", 75 );
    }

    function bspkn_form_admin(){

        global $wpdb;
        $register_users_table = $wpdb->prefix . 'bspkn_register';
        $users = $wpdb->get_results("SELECT * FROM $register_users_table");


        echo '<div class="wrap"><h1 style="margin-bottom:20px"> BSPK FORM</h1>';
        echo '<table class="wp-list-table widefat fixed striped">';
        echo '<thead>
                <tr>
                    <th width="20%">complete Name</th>
                    <th width="20%">email</th>
                    <th>country</th>
                    <th>region</th>
                    <th>city</th>
                    <th>phone</th>
                </tr>
            </thead>';

        echo '<tbody id="the-list">';

        foreach ($users as $user) {
           $completeName = esc_textarea( $user->completeName);
           $email = esc_textarea( $user->email);
           $country =  esc_textarea($user->country);
           $region =  esc_textarea($user->region);
           $city =  esc_textarea($user->city);
           $phone = (int)$user->phone;
           $skills = esc_textarea( $user->skills);

           echo "<tr>
                    <td><a href='#' title='$skills'>$completeName</a></td>
                    <td>$email</td>
                    <td>$country</td>
                    <td>$region</td>
                    <td>$city</td>
                    <td><a href='tel:$phone' class='button-primary'>Call Now</a></td>
                </tr>";
        }

        echo '</tbody></table></div>';
}