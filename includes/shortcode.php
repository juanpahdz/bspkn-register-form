<?php

add_shortcode( 'bspkn-register-form', 'bspkn_register_shortcode');
function bspkn_register_shortcode(){

    require_once BSPKFORM_PLUGIN_DIR . '/includes/controller.php';
    
    bspkn_insert_data();
    ob_start();

    ?>

        <form action="<?php get_the_permalink(); ?>" method="post">

            <h4>Inscription Form</h4>

            <?php  wp_nonce_field( 'bspkn-register-form', 'bspkn-form_nonce')?> <!-- Cross-site request forgery -->
            
            <div class="mb-3">
                <label for="completeName" class="form-label">Complete Name</label>
                <input name="completeName" type="text" class="form-control" id="completeName" placeholder="Jhon Alejo" required>
            </div>

            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input name="email" type="email" class="form-control" id="email" placeholder="me@example.com" required>
            </div>

            <label for="country" class="form-label">Where are you located?</label>

            <div class="input-group mb-3">
                <select name="country" id="country" class="form-select mb-3" aria-label=".form-select-lg" required>
                    <option value="" disabled selected>Country</option>
                </select>

                <select name="region" id="region" class="form-select mb-3" aria-label=".form-select-lg" required>
                    <option value="" disabled selected>State</option>
                </select>

                <select name="city" id="city" class="form-select mb-3" aria-label=".form-select-lg">
                    <option value="" disabled selected>City</option>
                </select>
            </div>

            <div class="input-group mb-3">
                <div class="input-group mb-3">
                    <span id="phone-code-number" class="input-group-text">+57</span>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone number" required>
                </div>
            </div>

            <div class="mb-3">
                <label for="skills" class="form-label">Let us know more about you and your skills</label>
                <textarea name="skills" class="form-control" id="skills" rows="3" maxlength="150" required></textarea>
            </div>
            
            <button type="submit" class="btn btn-primary">Send</button>
        </form>

    <?php
    return ob_get_clean();
}