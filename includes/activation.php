<?php

register_activation_hook( BPKFORM_FILE_URL, 'bspkn_register_form_init' );
function bspkn_register_form_init(){

    global $jal_db_version;
    $jal_db_version = '0.1';

    global $wpdb, $table_prefix;
    $register_users_table = $table_prefix . 'bspkn_register';
    $charset_collate = $wpdb->get_charset_collate();

    $query = "CREATE TABLE IF NOT EXISTS $register_users_table (
        id mediumint(9) AUTO_INCREMENT, 
        completeName varchar(40) NOT NULL, 
        email varchar(100) NOT NULL, 
        country varchar(50) NOT NULL,
        region varchar(50) NOT NULL,
        city varchar(50) NOT NULL,
        phone mediumint(9) NOT NULL,
        skills varchar(100) NOT NULL,
        UNIQUE (id)
        ) $charset_collate";

    require_once ABSPATH . 'wp-admin/includes/upgrade.php';

    dbDelta( $query );

    add_option( 'jal_db_version', $jal_db_version );
}

register_deactivation_hook( BPKFORM_FILE_URL, 'bspkn_register_form_disable' );
function bspkn_register_form_disable(){

}

if (BPKFORM_BOOTSTRAP){
    add_action('wp_head', 'bpskn_add_bootstrap');
    function bpskn_add_bootstrap(){
    echo '<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">';
    }
    
    add_action('wp_footer', 'bpskn_add_bootstrap_script');
    function bpskn_add_bootstrap_script(){
    echo '<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>">';
    }
}