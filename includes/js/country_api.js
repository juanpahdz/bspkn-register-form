const ApiToken = "8OsYhZMokZ384w1FOhqQY5fn8M9KwsuqEa30LeZ3ZzzfGLrPNr6gisp5YNKsusn6-p8" 
const Email = "juanpahdz01@gmail.com"
const xhttps = new XMLHttpRequest();

xhttps.open('GET', 'https://www.universal-tutorial.com/api/getaccesstoken', true);
xhttps.setRequestHeader( "api-token", ApiToken)
xhttps.setRequestHeader( "user-email", Email)


xhttps.onload = function () {
    if (xhttps.readyState === xhttps.DONE) {
        if (xhttps.status === 200) {
            const urlGet = JSON.parse(xhttps.response);
            const token = "Bearer " + urlGet.auth_token
            myObj.init(token);
        }
    }
};

xhttps.send()

var myObj = 
{
    init:function(token){
        var that = this;
        this.load_country(token);

        document.getElementById('country').addEventListener('change', function(){
            document.getElementById('region').innerHTML = '<option value="" disabled selected>State</option>';
            document.getElementById('city').innerHTML = '<option value="" disabled selected>City</option>';
            that.load_state(this.value, token)
            that.load_phone_code(this.value, token)
        })

        document.getElementById('region').addEventListener('change', function(){
            document.getElementById('city').innerHTML = '<option value="" disabled selected>City</option>';
            that.load_city(this.value, token)
        })

    },
    load_country:function(token){
        var xhr = new XMLHttpRequest();

        
        xhr.open('GET', 'https://www.universal-tutorial.com/api/countries/', true);
        xhr.setRequestHeader(
            "Authorization", token,
        )

        xhr.onload = function()
        {
            let countries = JSON.parse(xhr.responseText);
            countries.forEach( function(value)
            {
                let op = document.createElement('option');
                op.innerText = value.country_name;
                op.setAttribute('value', value.country_name);
                document.getElementById('country').appendChild(op)
            });
        }
        xhr.send();
    },
    load_state:function(country_name, token){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://www.universal-tutorial.com/api/states/' + country_name, true);
        xhr.setRequestHeader(
            "Authorization", token,
        )

        xhr.onload = function()
        {
            var countries = JSON.parse(xhr.responseText);
            countries.forEach( function(value)
            {
                let op = document.createElement('option');
                op.innerText = value.state_name;
                op.setAttribute('value', value.state_name);
                document.getElementById('region').appendChild(op)
            });
        }
        xhr.send();
    },
    load_city:function(state_name, token){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://www.universal-tutorial.com/api/cities/' + state_name, true);
        xhr.setRequestHeader(
            "Authorization", token,
        )

        xhr.onload = function()
        {
            var countries = JSON.parse(xhr.responseText);
            countries.forEach( function(value)
            {
                let op = document.createElement('option');
                op.innerText = value.city_name;
                op.setAttribute('value', value.city_name);
                document.getElementById('city').appendChild(op)
            });
        }
        xhr.send();
    },

    load_phone_code:function(code, token){
        var xhr = new XMLHttpRequest();

        xhr.open('GET', 'https://www.universal-tutorial.com/api/countries/', true);
        xhr.setRequestHeader(
            "Authorization", token,
        )
        xhr.onload = function()
        {
            var countries = JSON.parse(xhr.responseText);
            countries.forEach( function(value)
            {

              if(value.country_name == code){
                document.getElementById('phone-code-number').innerText = "+" + value.country_phone_code
              }

            });
        }
        xhr.send();
    }
}

document.addEventListener("DOMContentLoaded", function() {
    
  });