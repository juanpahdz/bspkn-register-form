<?php

function bspkn_insert_data(){
    
    global $wpdb;

    if(!empty($_POST) 
        && isset($_POST['completeName'])
        && $_POST['email'] != ''
        && $_POST['country'] != ''
        && $_POST['region'] != ''
        && $_POST['city'] != ''
        && $_POST['phone'] != ''
        && $_POST['skills'] != ''
        )
        {    
            $register_users_table = $wpdb->prefix . 'bspkn_register';
            $completeName = sanitize_text_field($_POST['completeName']);
            $email = $_POST['email'];
            $country =  sanitize_text_field($_POST['country']);
            $region =  sanitize_text_field($_POST['region']);
            $city =  sanitize_text_field($_POST['city']);
            $phone =  (int)$_POST['phone'];
            $skills = sanitize_text_field($_POST['skills']);

            $result = $wpdb->insert(
                $register_users_table, 
                array(
                    'completeName' => $completeName,
                    'email' => $email,
                    'country' => $country,
                    'region' => $region,
                    'city' => $city,
                    'phone' => $phone,
                    'skills' => $skills,
                    )
            );
        }

}