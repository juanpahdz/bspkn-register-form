<?php
/** 
    * Plugin Name: Bspkn Register Form
    * Author: Juan Pablo Hernandez
    * Description: Custon Form Plugin using a ShordCode [bspkn-register-form]
    * Version: 0.0.1
**/

define( 'BSPKFORM_PLUGIN', __FILE__ );

define( 'BSPKFORM_PLUGIN_DIR', untrailingslashit( dirname( BSPKFORM_PLUGIN ) ) );

define( 'BPKFORM_BOOTSTRAP', true );

define('BPKFORM_FILE_URL', __FILE__);

require_once  BSPKFORM_PLUGIN_DIR . '/includes/activation.php';

require_once  BSPKFORM_PLUGIN_DIR . '/includes/shortcode.php';

require_once BSPKFORM_PLUGIN_DIR . '/includes/country_rest_api.php';

require_once BSPKFORM_PLUGIN_DIR . '/admin/admin.php';
