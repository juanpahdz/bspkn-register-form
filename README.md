# BSPKN REGISTER FORM

BSPKN REGISTER FORM is a WordPress Plugin.

## Installation

Download Zip Code and install it on your local or online WordPress Website.

## Usage

Use the shortcode to display the form on your website. All inscriptions will be showing on the WordPress BackOffice 
```bash
[bspkn-register-form]
```

If you already have Bootstrap on your website set 'BPKFORM_BOOTSTRAP' on bspk-register-form.php

```php
define( 'BPKFORM_BOOTSTRAP', false);
```

## REST API for countries, states, and cities

https://www.universal-tutorial.com/rest-apis/free-rest-api-for-country-state-city